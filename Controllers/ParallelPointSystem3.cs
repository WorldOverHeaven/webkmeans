﻿namespace ConsoleAppKMeans;

public class ParallelPointSystem3
{
    private List<Cluster> _clusters;
    private List<Point> _points;
    private int _dimension;
    public int Dimension => _dimension;
    private int _clustersAmount;
    private readonly int _iterations = 5000;

    
    public ParallelPointSystem3(int dimension, int clustersAmount) {
        if (dimension <= 0) {
            throw new Exception("Dimension should be > 0");
        }
        _dimension = dimension;

        if (clustersAmount <= 0) {
            throw new Exception("Amount of clusters should be > 0");
        }
        _clustersAmount = clustersAmount;

        _clusters = new List<Cluster>();
        _points = new List<Point>();
    }
    
    public void InitPoints(List<List<Double>> inputData) {
        int id = 0;
        for (var i = 0; i < inputData.Count; ++i) {
            if (inputData[i].Count != _dimension) {
                throw new Exception("Met point with unknown dimension");
            }

            _points.Add(new Point(id, inputData[i]));
            id += 1;
        }
    }
    
    public void GeneratePoints(int count)
    {
        var rand = new Random();
        for (var i = 0; i < count; ++i)
        {
            var coords = new List<double>(_dimension);
            // coords.Add(rand.NextDouble());
            // coords.Add(rand.NextDouble());
            for (var j = 0; j < _dimension; ++j)
            {
                coords.Add(rand.NextDouble());
            }
            _points.Add(new Point(i, coords));
        }
    }
    
    private void InitClusters() {
        var rand = new Random();

        for (int i = 0; i < _clustersAmount; i ++) {
            var nCluster = new Cluster(_dimension);
            nCluster.SetCenter(_points[rand.Next(_points.Count)]);
            _clusters.Add(nCluster);
        }

        Console.WriteLine(_clusters.Count);
    }

    private int FindMinDistance(ConcurrentList<double> distances) {
        var minEl = distances[0];
        var ind = 0;

        for (var i = 1; i < _clustersAmount; i++) {
            if (minEl > distances[i]) {
                minEl = distances[i];
                ind = i;
            }
        }

        return ind;
    }
    
    private int FindMinDistance(List<double> distances) {
        var minEl = distances[0];
        var ind = 0;

        for (var i = 1; i < _clusters.Count; i++) {
            if (minEl > distances[i]) {
                minEl = distances[i];
                ind = i;
            }
        }

        return ind;
    }


    private void ClearClusters() {
        for (var i = 0; i < _clusters.Count; ++i) {
            _clusters[i].Clear();
        }
    }
    
    private bool ClustersCentresArentMoving() {
        for (var i = 0; i < _clustersAmount; ++i) {
            if (! _clusters[i].PrevCenterEqToCur()) {
                return false;
            }
        }
        return true;
    }

    public Dictionary<String, List<List<Double>>> GetClustersData() {
        var res = new Dictionary<String, List<List<Double>>>();

        for (var i = 0; i < _clustersAmount; ++i) {
            var attachedPoints = _clusters[i].GetAttachedPoints();
            var r = new List<List<Double>>();

            for (var j = 0; j < attachedPoints.Count; ++j) {
                r.Add(attachedPoints[j].GetAllComponents());
            }

            res.Add(i.ToString(), r);
        }

        var tmp = new List<List<Double>>();

        for (var j = 0; j < _clustersAmount; ++j) {
            tmp.Add(_clusters[j].GetCurrentCenter().GetAllComponents());
        }
        res.Add("centers", tmp);
        return res;
    }
    
    private async Task AttachPoint(int i)
    {
        var ii = (int)i;
        var distance = Point.Distance(_points[ii], _clusters[0].GetCurrentCenter());
        int ind = 0;
        for (var j = 1; j < _clustersAmount; j++) {
            var dst = Point.Distance(_points[ii], _clusters[j].GetCurrentCenter());
            if (distance > dst)
            {
                distance = dst;
                ind = j;
            }
        }
        _clusters[ind].Attach(_points[ii]);
    }

    public async Task<Dictionary<String, List<List<Double>>>> SplitOnClustersParallel() {
        if (_points.Count == 0) {
            throw new Exception("No points to cluster");
        }

        // Можно выполнять параллельно
        InitClusters();

        for (var i = 0; i < _iterations; i++)
        {
            var customThreadPool = new CustomThreadPool();
            // Можно выполнять параллельно, для этого нужно поменять функцию CleearClusters
            ClearClusters();

            // Можно выполнять параллельно

            var listTasks = new List<Task>();

            for (var ii = 0; ii < _points.Count; ++ii)
            {
                await customThreadPool.Add(new Func<int, Task>(AttachPoint), ii);
            }

            await customThreadPool.AwaitAll();
            
            // Можно выполнять параллельно
            for (var ii = 0; ii < _clusters.Count; ++ii) {
                _clusters[ii].MoveCenter();
            }

            if (ClustersCentresArentMoving()) {
                Console.WriteLine(i);
                break;
            }
        }
        return GetClustersData();
    }
}