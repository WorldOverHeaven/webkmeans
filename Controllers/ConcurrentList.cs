﻿using System.Collections.Concurrent;

namespace ConsoleAppKMeans;

public class ConcurrentList<T>
{
    private ConcurrentDictionary<int, T> _list;
    public int Count => _list.Count;

    public ConcurrentList()
    {
        _list = new ConcurrentDictionary<int, T>();
    }

    public void Add(T value)
    {
        _list.AddOrUpdate(Count, value, (i, arg2) => value);
    }
    
    public T this[int i] => _list[i];

    public void Update(int id, T value)
    {
        _list.AddOrUpdate(id, value, (i, arg2) => value);
    }
}