﻿using System.Diagnostics;
using ConsoleAppKMeans;
using Microsoft.AspNetCore.Mvc;
using WebKMeans.Models;
using ConsoleAppKMeans;

namespace WebKMeans.Controllers;

public class HomeController : Controller
{
    private int _id = 0;
    
    private readonly ILogger<HomeController> _logger;

    public HomeController(ILogger<HomeController> logger)
    {
        _logger = logger;
    }

    public IActionResult Index(string a = "10", string b  = "20")
    {
        var mod = new List<string> {a, b};
        Console.WriteLine($"a = {a}, b = {b}");
        return View(mod);
    }
    
    public IActionResult Canvas(int count = 1000, int clusterAmount = 10)
    {
        int dimension = 2;
        var pointSystem = new PointSystem(dimension, clusterAmount);
        pointSystem.GeneratePoints(count);
        Stopwatch stopwatch = Stopwatch.StartNew();
        var clusters = pointSystem.SplitOnClusters();
        stopwatch.Stop();
        var time = stopwatch.ElapsedMilliseconds;
        Console.WriteLine($"time = {time}");
        return View(clusters);
    }
    
    public IActionResult CanvasParallel(int count = 1000, int clusterAmount = 10)
    {
        var id = _id;
        _id += 1;
        
        
        int dimension = 2;
        var pointSystem = new ParallelPointSystem2(dimension, clusterAmount);
        pointSystem.GeneratePoints(count);
        
        Stopwatch stopwatch = Stopwatch.StartNew();
        
        Rabbit.SendTask(pointSystem, id);
        
        var clusterData = new ClusterData();

        Rabbit.ReceiveTaskResult(clusterData, id);

        var clusters = clusterData.clusterData;
        
        stopwatch.Stop();
        
        var time = stopwatch.ElapsedMilliseconds;
        Console.WriteLine($"time = {time}");
        return View(clusters);
    }
    
    public IActionResult CanvasParallelThreadPool(int count = 1000, int clusterAmount = 10)
    {
        int dimension = 2;
        var pointSystem = new ParallelPointSystem2(dimension, clusterAmount);
        pointSystem.GeneratePoints(count);
        Stopwatch stopwatch = Stopwatch.StartNew();
        var clusters = pointSystem.SplitOnClustersParallel();
        stopwatch.Stop();
        var time = stopwatch.ElapsedMilliseconds;
        Console.WriteLine($"time = {time}");
        return View(clusters);
    }

    public IActionResult Privacy()
    {
        return View();
    }

    [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
    public IActionResult Error()
    {
        return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
    }
}