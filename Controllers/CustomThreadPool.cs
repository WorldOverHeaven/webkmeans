﻿namespace ConsoleAppKMeans;
using System.Collections.Concurrent;
public class CustomThreadPool
{
    private readonly int _threads;
    private ConcurrentQueue<Task> _tasks;

    public CustomThreadPool()
    {
        _threads = 16;
        _tasks = new ConcurrentQueue<Task>();
    }
    
    public async Task Add(Func<int, Task> method, int arg)
    {
        _tasks.Enqueue(method(arg));
        if (_tasks.Count == _threads)
        {
            await AwaitOne();
        }
    }

    public async Task AwaitOne()
    {
        Task result;
        if (_tasks.TryDequeue(out result))
        {
            await result;
        }
        else
        {
            Console.WriteLine("dequeue failed");
        }
    }

    public async Task AwaitAll()
    {
        foreach(var i in _tasks)
        {
            await i;
        }
        _tasks.Clear();
    }
}



// namespace ConsoleAppKMeans;
//
// public class CustomThreadPool
// {
//     private readonly int _threads;
//     private int _inWork;
//     private List<Task> _tasks;
//
//     public CustomThreadPool()
//     {
//         _threads = 20;
//         _inWork = 0;
//         _tasks = new List<Task>();
//     }
//     
//     public async Task Add(Func<int, Task> method, int arg)
//     {
//         _tasks.Add(method(arg));
//         _inWork += 1;
//         if (_inWork == _threads)
//         {
//             await AwaitAll();
//         }
//     }
//
//     public async Task AwaitAll()
//     {
//         _inWork = 0;
//         //Console.WriteLine($"awaitAll, {_tasks.Count}");
//         foreach(var i in _tasks)
//         {
//             await i;
//         }
//         _tasks.Clear();
//         //Console.WriteLine($"awaitAll clear, {_tasks.Count}");
//     }
//}