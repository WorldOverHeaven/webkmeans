﻿using System.Runtime.Serialization.Formatters.Binary;
using RabbitMQ.Client.Events;

namespace ConsoleAppKMeans;

using System;
using RabbitMQ.Client;
using System.Text;

[Serializable]
public class ClusterData
{
    public Dictionary<String, List<List<Double>>> clusterData;

    public ClusterData()
    {
        clusterData = new Dictionary<string, List<List<double>>>();
    }
}

public class Rabbit
{

    public static byte[] ObjectToByteArray(Object obj)
    {
        BinaryFormatter bf = new BinaryFormatter();
        using (var ms = new MemoryStream())
        {
            bf.Serialize(ms, obj);
            return ms.ToArray();
        }
    }
    
    public static Object ByteArrayToObject(byte[] arrBytes)
    {
        using (var memStream = new MemoryStream())
        {
            var binForm = new BinaryFormatter();
            memStream.Write(arrBytes, 0, arrBytes.Length);
            memStream.Seek(0, SeekOrigin.Begin);
            var obj = binForm.Deserialize(memStream);
            return obj;
        }
    }

    public static void S()
    {
        var message = new ParallelPointSystem2(2, 10);
        message.GeneratePoints(100);
        
        Calculate(message);
        
        var clusterData = new ClusterData();
        clusterData.clusterData = message.GetClustersData();
                
        SendTaskResult(clusterData, 12);
    }

    public static void SendAndReceive()
    {
        int dimension = 2;
        int clusterAmount = 10;
        int count = 1000;
        int id = 7;
        
        var pointSystem = new ParallelPointSystem2(dimension, clusterAmount);
        pointSystem.GeneratePoints(count);

        SendTask(pointSystem, id);
        
        Console.WriteLine("AAAA");

        var clusterData = new ClusterData();

        ReceiveTaskResult(clusterData, id);

        Console.WriteLine(clusterData.clusterData["centers"]);

        foreach (var i in clusterData.clusterData["centers"])
        {
            foreach (var j in i)
            {
                Console.WriteLine(j);
            }
        }
    }

    public static void Send()
    {
        int dimension = 2;
        int clusterAmount = 2;
        int count = 10;
        
        var pointSystem = new ParallelPointSystem2(dimension, clusterAmount);
        pointSystem.GeneratePoints(count);
        
        SendTask(pointSystem, 10);
    }
    
    public static void SendTask(ParallelPointSystem2 pointSystem, int id)
    {
        var factory = new ConnectionFactory() { HostName = "localhost" };
        using (var connection = factory.CreateConnection())
        using (var channel = connection.CreateModel())
        {
            channel.QueueDeclare(queue: "allTasks",
                durable: false,
                exclusive: false,
                autoDelete: false,
                arguments: null);
            
            var body = ObjectToByteArray((pointSystem, id));

            channel.BasicPublish(exchange: "",
                routingKey: "allTasks",
                basicProperties: null,
                body: body);
            Console.WriteLine($"Sent {id}");
        }
    }
    
    public static void ReceiveTask()
    {
        //Send();
        var factory = new ConnectionFactory() { HostName = "localhost" };
        using(var connection = factory.CreateConnection())
        using(var channel = connection.CreateModel())
        {
            channel.QueueDeclare(queue: "allTasks",
                durable: false,
                exclusive: false,
                autoDelete: false,
                arguments: null);

            var consumer = new EventingBasicConsumer(channel);
            consumer.Received += (model, ea) =>
            {
                var body = ea.Body.ToArray();
                var message = ((ParallelPointSystem2, int))ByteArrayToObject(body);
                Console.WriteLine($"Received {message.Item2}");
                
                Calculate(message.Item1);

                Console.WriteLine($"Send from ReceiveTask start id = {message.Item2}");
                
                var clusterData = new ClusterData();
                clusterData.clusterData = message.Item1.GetClustersData();
                
                SendTaskResult(clusterData, message.Item2);

                Console.WriteLine($"Send from ReceiveTask end id = {message.Item2}");
            };
            channel.BasicConsume(queue: "allTasks",
                autoAck: true,
                consumer: consumer);

            Console.WriteLine(" Press [enter] to exit.");
            Console.ReadLine();
        }
    }

    public static void Calculate(ParallelPointSystem2 pointSystem)
    {
        pointSystem.SplitOnClustersParallel();
    }

    public static void SendTaskResult(ClusterData clusterData, int id)
    {
        var factory = new ConnectionFactory() { HostName = "localhost" };
        using (var connection = factory.CreateConnection())
        using (var channel = connection.CreateModel())
        {
            channel.QueueDeclare(queue: $"task{id}",
                durable: false,
                exclusive: false,
                autoDelete: false,
                arguments: null);
            
            var body = ObjectToByteArray(clusterData);

            channel.BasicPublish(exchange: "",
                routingKey: $"task{id}",
                basicProperties: null,
                body: body);
            Console.WriteLine($"Sent {id}");
        }
    }
    
    public static void ReceiveTaskResult(ClusterData clusterData, int id)
    {
        var flag = false;
        var factory = new ConnectionFactory() { HostName = "localhost" };
        using(var connection = factory.CreateConnection())
        using(var channel = connection.CreateModel())
        {
            channel.QueueDeclare(queue: $"task{id}",
                durable: false,
                exclusive: false,
                autoDelete: false,
                arguments: null);

            var consumer = new EventingBasicConsumer(channel);
            consumer.Received += (model, ea) =>
            {
                var body = ea.Body.ToArray();
                var message = (ClusterData)ByteArrayToObject(body);
                clusterData.clusterData = message.clusterData;
                Console.WriteLine($"Received {id}");
                flag = true;
            };
            channel.BasicConsume(queue: $"task{id}",
                autoAck: true,
                consumer: consumer);

            //Console.WriteLine("BBB");
            while (!flag)
            {
                //Console.WriteLine("aaa");
                Thread.Sleep(100);
            }
            
            //Console.WriteLine("End ReceiveTaskResult");
            //Console.ReadLine();
        }
    }
}