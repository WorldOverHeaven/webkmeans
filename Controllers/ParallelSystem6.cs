﻿namespace ConsoleAppKMeans;

public class ParallelPointSystem6
{
    private ConcurrentList<Cluster> _clusters;
    private List<Point> _points;
    private int _dimension;
    public int Dimension => _dimension;
    private int _clustersAmount;
    private readonly int _iterations = 5000;

    
    public ParallelPointSystem6(int dimension, int clustersAmount) {
        if (dimension <= 0) {
            throw new Exception("Dimension should be > 0");
        }
        _dimension = dimension;

        if (clustersAmount <= 0) {
            throw new Exception("Amount of clusters should be > 0");
        }
        _clustersAmount = clustersAmount;

        _clusters = new ConcurrentList<Cluster>();
        _points = new List<Point>();
    }
    
    public void InitPoints(List<List<Double>> inputData) {
        int id = 0;
        for (var i = 0; i < inputData.Count; ++i) {
            if (inputData[i].Count != _dimension) {
                throw new Exception("Met point with unknown dimension");
            }

            _points.Add(new Point(id, inputData[i]));
            id += 1;
        }
    }
    
    public void GeneratePoints(int count)
    {
        var rand = new Random();
        for (var i = 0; i < count; ++i)
        {
            var coords = new List<double>(_dimension);
            // coords.Add(rand.NextDouble());
            // coords.Add(rand.NextDouble());
            for (var j = 0; j < _dimension; ++j)
            {
                coords.Add(rand.NextDouble());
            }
            _points.Add(new Point(i, coords));
        }
    }
    
    private void InitClusters() {
        var rand = new Random();

        for (int i = 0; i < _clustersAmount; i ++) {
            var nCluster = new Cluster(_dimension);
            nCluster.SetCenter(_points[rand.Next(_points.Count)]);
            _clusters.Add(nCluster);
        }

        Console.WriteLine(_clusters.Count);
    }
    

    private void ClearClusters() {
        for (var i = 0; i < _clusters.Count; ++i) {
            // WaitCallback wc = new WaitCallback(ClearHelp);
            // ThreadPool.QueueUserWorkItem(wc, i);
            _clusters[i].Clear();
        }
        
        //ThreadPoolWaiting();
    }

    private void ClearHelp(object i) 
    {
        _clusters[(int)i].Clear();
    }
    
    private bool ClustersCentresArentMoving() {
        for (var i = 0; i < _clustersAmount; ++i) {
            if (! _clusters[i].PrevCenterEqToCur()) {
                return false;
            }
        }
        return true;
    }

    private void AttachPoint(object i)
    {
        var ii = (int)i;
        var distance = Point.Distance(_points[ii], _clusters[0].GetCurrentCenter());
        int ind = 0;
        for (var j = 1; j < _clustersAmount; j++) {
            var dst = Point.Distance(_points[ii], _clusters[j].GetCurrentCenter());
            if (distance > dst)
            {
                distance = dst;
                ind = j;
            }
        }
        _clusters[ind].Attach(_points[ii]);
    }
    
    public Dictionary<String, List<List<Double>>> GetClustersData() {
        var res = new Dictionary<String, List<List<Double>>>();

        for (var i = 0; i < _clustersAmount; ++i) {
            var attachedPoints = _clusters[i].GetAttachedPoints();
            var r = new List<List<Double>>();

            for (var j = 0; j < attachedPoints.Count; ++j) {
                r.Add(attachedPoints[j].GetAllComponents());
            }

            res.Add(i.ToString(), r);
        }

        var tmp = new List<List<Double>>();

        for (var j = 0; j < _clustersAmount; ++j) {
            tmp.Add(_clusters[j].GetCurrentCenter().GetAllComponents());
        }
        res.Add("centers", tmp);
        return res;
    }

    public Dictionary<String, List<List<Double>>> SplitOnClustersParallel() {
        if (_points.Count == 0) {
            throw new Exception("No points to cluster");
        }
        
        // Можно выполнять параллельно
        InitClusters();

        //ThreadPool.SetMaxThreads(16, 16);
        for (var i = 0; i < _iterations; i++) {
            // Можно выполнять параллельно, для этого нужно поменять функцию CleearClusters
            ClearClusters();
            
            for (var ii = 0; ii < _points.Count; ++ii)
            {
                WaitCallback wc = new WaitCallback(AttachPoint);
                ThreadPool.QueueUserWorkItem(wc, ii);
            }
            
            // int workerThreads;
            // int portThreads;
            // ThreadPool.GetMaxThreads(out workerThreads, out portThreads);
            // Console.WriteLine($"workerThreads, portThreads = {workerThreads}, {portThreads}");

            while (ThreadPool.PendingWorkItemCount != 0)
            {
                Thread.Sleep(10);
            }

            // Можно выполнять параллельно
            for (var ii = 0; ii < _clusters.Count; ++ii) {
                WaitCallback wc = new WaitCallback(MoveCenterHelp);
                ThreadPool.QueueUserWorkItem(wc, ii);
            }

            while (ThreadPool.PendingWorkItemCount != 0)
            {
                Thread.Sleep(10);
            }

            if (ClustersCentresArentMoving()) {
                Console.WriteLine($"iterations = {i}");
                break;
            }
        }
        return GetClustersData();
    }

    private void MoveCenterHelp(object ii)
    {
        _clusters[(int)ii].MoveCenter();
    }

    private void ThreadPoolWaiting()
    {
        while (ThreadPool.PendingWorkItemCount != 0)
        {
            Thread.Sleep(10);
        }
    }
}