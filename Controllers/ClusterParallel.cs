﻿using System.Collections.Concurrent;

namespace ConsoleAppKMeans;

public class ClusterParallel
{
    public ConcurrentDictionary<int, Point> _points;
    private Point _currentCenter;
    private Point _prevCenter;
    private int _dimension;
    private double _precision = 0.001;
    public ConcurrentDictionary<int, Point> Points => _points;
    
    public ClusterParallel(int dimension) {
        _dimension = dimension;
        _points = new ConcurrentDictionary<int, Point>();
        _currentCenter = GenerateInitialCenter(dimension);
        _prevCenter = GenerateInitialCenter(dimension);
    }
    
    public void Clear() {
        _points = new ConcurrentDictionary<int, Point>();
    }

    
    private Point GenerateInitialCenter(int dimension) {
        var coords = new List<Double>();
        for (var i = 0; i < dimension; i++) {
            coords.Add(0.0);
        }

        return new Point(0, coords);
    }
    
    public void Print() {
        _currentCenter.Print();
    }
    
    public void SetCenter(Point newCenter) {
        _prevCenter = _currentCenter;
        _currentCenter = newCenter;
    }
    
    public Point GetCurrentCenter() {
        return _currentCenter;
    }
    
    public bool PrevCenterEqToCur() {
        Console.WriteLine(Point.Distance(_currentCenter, _prevCenter));
        return Point.Distance(_currentCenter, _prevCenter) < _precision;
    }
    
    public void MoveCenter() {
        if (_points.Count == 0) {
            SetCenter(_currentCenter);
            return;
        }

        List<double> masses = new List<double>(_dimension);
        for (var i = 0; i < _dimension; i++) {
            masses.Add(0.0);
        }
        
        foreach (var i in _points.Keys) {
            for (var j = 0; j < _dimension; j++) {
                masses[j] += _points[i][j];
            }
        }

        for (var i = 0; i < _dimension; i++) {
            masses[i] /=  _points.Count;
        }

        SetCenter(new Point(0, masses));
    }
    
    public List<Point> GetAttachedPoints() {
        return new List<Point>(_points.Values);
    }
    
    public void Attach(Point p) {
        if (p.Dimension != _dimension) {
            throw new Exception("You can put only points with same dimension in one cluster");
        }
        _points.AddOrUpdate(p.Id, p, (key, oldValue) => oldValue);
    }


}